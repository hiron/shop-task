export const theme = {
  colors: {
    primary: "#0edea4",
    secondary: "#000000",
    font: "#000000",
    secondaryFont: "#00000080", //rgba(0, 0, 0, 0.5),
    productMain: "#414141",
    buttonFont: "#ffffff",
  },
  grid: 12,
  layout: {
    xs: "768px",
    sm: "820px",
    md: "1080px",
  },
};
