import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Inter:wght@300;400;500;600;700&display=swap');

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-bos;
  }

  body {
    background: #fff;
    color: #000000;
    font-family: 'Inter', sans-serif;
    font-size: 1em;
    margin: 0;
  }

  img {
    max-width: 100%;
  }

  a{
    text-decoration: none;
    color: #000;
  }
  
`;
