import WooCommerceAPI from "woocommerce-api";

export const WooCommerce = new WooCommerceAPI({
  // url: "https://shop-interview.acrowd.se",
  // consumerKey: "ck_4c0d8a4f83c78831c200e39d1f371e92d419d863",
  // consumerSecret: "cs_1eb6c96b9a32942b52a868da3ad28698b15873ff",
  url: process.env.baseUrl,
  consumerKey: process.env.consumerKey,
  consumerSecret: process.env.consumerSecret,
  wpAPI: true,
  version: "wc/v3",
});
