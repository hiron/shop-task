import styled from "styled-components";

export const StyledHeader = styled.header`
  color: ${({ theme }) => theme.colors.font};
  padding-top: 10px;
  padding-bottom: 56px;

  & > p {
    visibility: ${({ action }) => action ? 'visible' : 'hidden'};
    margin: 20px 0px;
    font-size: 18px;
    font-weight: 400;
    cursor: pointer;

    &:hover{
      font-weight: 700;
    }
  }

  &>div{
    font-size: 48px;
    font-weight: 400;
    text-align: center;
    margin-bottom: 38px;
  }

  & > nav{
    margin: auto 0;
    display: flex;
    box-sizing: border-box;
    flex-wrap: wrap;
    flex-items: center;
    text-align: center;
    gap: 20px;
    justify-content: center;
    font-size: 18px;
    font-weight: 400;
  }

  & a{
    color: ${({ theme }) => theme.colors.font};
    text-decoration: none;
    display: inline-block;
  }

  & a:hover{
    color: ${({ theme }) => theme.colors.primary}
  }

`;