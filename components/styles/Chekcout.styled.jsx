import styled from "styled-components";
import { Container } from "./Container.styled";

export const StyledCheckout = styled(Container)`
    width: 800px;

    & h2{
        padding-top: 80px;
        padding-bottom: 50px;
      }

    & label{
        width: 100%;
        display: inline-block;
        box-sizing: border-box;
    }

    & input{
        width: 100%;
        border: 2px solid ${({ theme }) => theme.colors.secondary};
        padding: 15px 22px;
        margin: 8px 0px;
        box-sizing: border-box;
    }

    & table{
        width: 100%;
        border-collapse: collapse;
        margin-top: 84px;
        margin-bottom: 60px;
    
        &>thead{
            background-color: #F7F7F7;
    
            & th{
                padding: 16px 0;
                font-size: 16px;
                font-weight: 400;
                line-height: 24px;
            }
        }

        &>tbody{
            & span{
                font-weight: 700;
            }
        }

        &>tfoot{
            font-weight: 700;
            & td{
                padding-top: 30px;
            }

            & td:nth-last-child(1){
                display: flex;
                justify-content: space-around
            }
        }
      }

    & table + div{
        display: flex;
        justify-content: space-around;
    }
`;