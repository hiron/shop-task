import styled from "styled-components";

export const Button = styled.button`
  color: ${({ theme }) => theme.colors.buttonFont};
  background: ${({ theme }) => theme.colors.secondary};
  border: 0px;
  padding: 15px 30px;
  font-weight: 400;
  font-size: 16px;
  line-height: 19px;
  vertical-align: middle;
  display: inline-block;

  &:hover{
    opacity: 0.9;
    cursor: pointer
  }

  &:active{
    transform: scale(0.99);
  }

  &:disabled{
    opacity: 0.6;
  }
`;