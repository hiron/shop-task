import styled from "styled-components";

export const StyledCartItem = styled.tr`
  &>td{
    padding-top: 30px;

    &>div{
        display: flex;
        justify-content:space-around;

        &>span{
            border: 0px;
            vertical-align: middle;
            padding: 0 10px;
            cursor: pointer;
            &:hover{
                transform: scale(1.2)
            }
        }
    }

    & div.product{
        justify-content:start;
        &>div{
            padding-right: 48px;
            margin: auto 0;
            font-weight: 700;
            
            @media(max-width:${({ theme }) => theme.layout.xs}){
                &:nth-child(1){
                    display: none;
                }
            }
        }

        
    }

    & div.qty{
        border: 2px solid ${({ theme }) => theme.colors.button};
        padding: 7px 0;
        max-width: 120px;
        margin: auto;

        @media(max-width:${({ theme }) => theme.layout.xs}){
           
            margin: auto 50px;
            max-width: 100%;
        }
    }

  }
`;