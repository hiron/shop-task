import styled from "styled-components";

export const ProductImage = styled.div`
  height: 580px;
  position: relative;
  width: 100%;
  display: block;
  background: ${({ theme }) => theme.colors.primary};
  &>div{
    z-index: 10000;
    position: absolute;
    right: 20px;
    top: 20px;
    color: red;
    background: #ff050591;
    padding: 10px;
    border-radius: 3px;
  }
`;