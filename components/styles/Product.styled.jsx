import styled from "styled-components";

export const StyledProduct = styled.div`
  padding-top: 120px;
  & main{
    padding-left: 110px;
    &>div{
        padding: 24px 0;

        &>p{
            color: ${({ theme }) => theme.colors.productMain};
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            text-align: justify;
            text-justify: inter-word;
        }
    }

    & ins,& bdi{
        text-decoration: none;
        font-weight: 400;
    }

    & del{
        font-weight: 400;
        padding-right: 10px;
        color: ${({ theme }) => theme.colors.secondaryFont};
        text-decoration: line-through;
        line-height: 100%;
    }

    &>span{
        border: 2px solid black;
        padding: 11px 10px;
        display: inline-block;


        &>span{
            vertical-align: middle;
            padding: 0 10px;
            cursor: pointer;
            &:hover{
                transform: scale(1.02)
            }
        }
        &>div{
            padding: 0 20px;
            display: inline-block;
        }
    }
  }

  & h3{
    padding-top: 170px;
    padding-bottom: 44px;
    font-weight: 700;
    font-size: 24px;
  }

 
  @media(max-width:${({ theme }) => theme.layout.xs}){
    padding-top: 80px;
    & main{
        padding-left: 0px;
    }
  }

`;