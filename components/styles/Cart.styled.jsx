import styled from "styled-components";
import { Container } from "./Container.styled";

export const StyledCart = styled(Container)`
  & h2{
    padding-top: 80px;
    padding-bottom: 70px;
  }

  & table{
    width: 100%;
    border-collapse: collapse;

    &>thead{
        background-color: #F7F7F7;

        & th{
            padding: 16px 0;
            font-size: 16px;
            font-weight: 400;
            line-height: 24px;
        }
    }
  }

  & span{
    display: flex;
    justify-content: end;
    border-top: 2px solid #F7F7F7;
    padding-top: 30px;
  }
`;