import styled from "styled-components";

export const StyledCard = styled.div`
    height: 370px;
    &:hover{
        box-shadow: 0px 20px 24px rgba(0, 0, 0, 0.04);
        transform: scale(1.02);
        cursor: pointer;
    }

    &>ins>span,&>span{
        position: relative;
        height: 274px;
        width: 100%;
        display: block;
        background: ${({ theme }) => theme.colors.primary};

        &>div{
            z-index: 1000;
            position: absolute;
            font-size: 12px;
            right: 10px;
            top: 10px;
            color: ${({theme})=> theme.colors.buttonFont};
            background: #ff050591;
            padding: 5px;
            border-radius: 3px;
        }
    }


    &>div{
        padding-top: 10px;
        &>span{
            font-weight: 700;
            line-height: 150%;
        }

        & ins,& bdi{
            text-decoration: none;
            font-weight: 400;
        }

        &>del{
            font-weight: 400;
            padding-right: 10px;
            color: ${({ theme }) => theme.colors.secondaryFont};
            text-decoration: line-through;
            line-height: 100%;
        }
    }
`;