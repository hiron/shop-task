import { currency } from "../common/helper";
import Image from 'next/image';
import { StyledCartItem } from "./styles/CartItem.styled";
import { useDispatch } from "react-redux";
import { deleteItem, updateQty } from "../features/cart/cartSlice";

export const CheckoutItem = ({ quantity, product }) => {
    const dispatch = useDispatch();
    return <StyledCartItem>
        <td>
            {product.name + " x "}
            <span>
                {quantity}
            </span>
        </td>
        <td>
            <div>
                {currency(product.price * quantity)}
            </div>
        </td>
    </StyledCartItem >;
}