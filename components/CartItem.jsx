import { currency } from "../common/helper";
import Image from 'next/image';
import { StyledCartItem } from "./styles/CartItem.styled";
import { useDispatch } from "react-redux";
import { deleteItem, updateQty } from "../features/cart/cartSlice";

export const CartItem = ({ quantity, product }) => {
    const dispatch = useDispatch();
    return <StyledCartItem>
        <td>
            <div className="product">
                <div><Image alt={product.images[0].name} width={121} height={118}
                    src={
                        product.images[0].src
                    }
                    objectFit='cover' objectPosition="center"
                ></Image>
                </div>
                <div>{product.name}</div>
            </div>
        </td>
        <td>
            <div>
                {currency(product.price)}
            </div>
        </td>
        <td>
            <div className="qty">
                <span className="material-icons" onClick={() => { dispatch(updateQty({ product_id: product.id, quantity: quantity - 1 })) }}>
                    remove
                </span>
                <div>
                    {quantity}
                </div>

                <span className="material-icons" onClick={() => { dispatch(updateQty({ product_id: product.id, quantity: quantity + 1 })) }}>
                    add
                </span>
            </div>
        </td>
        <td>
            <div>
                <div>
                    {currency(product.price * quantity)}
                </div>
                <span className="material-icons" onClick={() => { dispatch(deleteItem({ product_id: product.id })) }}>
                    clear
                </span>
            </div>
        </td>
    </StyledCartItem >;
}