import Head from 'next/head'

export const Meta = ({ title, description, img }) => {
    return (<Head>
        <title>{title || "Shop"}</title>
        <meta charset="UTF-8" />
        <meta name="description" content={description || "Shop"} />
        <meta name="keywords" content="Shop, Product, Interview" />
        {!!img &&
            <meta property="og:image" content={img} />
        }
        <meta property="og:title" content={title} />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>)
}