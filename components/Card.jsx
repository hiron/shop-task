import { StyledCard } from "./styles/Card.styled"
import Image from "next/image";
import { useTheme } from "styled-components";
import { currency } from "../common/helper";

export const Card = ({ product, priority }) => {
    const theme = useTheme();
    return <StyledCard>
        <span>
            <Image priority={priority} alt={product.images[0].name} src={product.images[0].src} layout="fill"
                sizes={"(min-width:" + theme.layout.md + ") 25vw,(min-width: " + theme.layout.sm + ") 50vw, 100vw"}
                objectFit='cover' objectPosition="center"
            />
            {product.on_sale ? <div>Sale</div> : ''}
        </span>
        <div> <span>
            {product.name.length > 45 ? product.name.substr(0, 45) + "..." : product.name}
        </span>
        </div>
        <div dangerouslySetInnerHTML={{__html: product.price_html }}>
            {/* <span className="regular-price">{currency(product.regular_price)}</span>{currency(product.price)} */}
        </div>
    </StyledCard >
}