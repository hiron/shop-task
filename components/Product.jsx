import Image from "next/image"
import Link from "next/link"
import { useRouter } from "next/router"
import { useState } from "react"
import { addItem } from "../features/cart/cartSlice"
import { Card } from "./Card"
import { Button } from "./styles/Button.styled"
import { Flex } from "./styles/Flex.styled"
import { StyledProduct } from "./styles/Product.styled"
import { ProductImage } from "./styles/productImage.styled"
import { useSelector, useDispatch } from "react-redux"
import { Meta } from "./Meta"

export const Product = ({ product, relatedProducts }) => {

    const { cart } = useSelector((state) => state);
    const dispatch = useDispatch();

    const [itemQuantity, setItemQuantity] = useState(1);
    const router = useRouter();
    const increment = () => {
        setItemQuantity(itemQuantity + 1);
    }
    const decrement = () => {
        if (itemQuantity > 1)
            setItemQuantity(itemQuantity - 1);
    }

    const addToCart = () => {
        // let cart = JSON.parse(localStorage.getItem('cart')|| JSON.stringify({ line_items: [] })); 
        // cart.line_items.push({ product_id: product.id, quantity: itemQuantity });
        // localStorage.setItem('cart', JSON.stringify(cart));
        dispatch(addItem({ product_id: product.id, quantity: itemQuantity, product: product }))
        router.push('/cart');
    }
    return (
        <StyledProduct>
            <Meta title={product.name} description={product.short_description} img={product.images[0].src}></Meta>
            <Flex container={true}>
                <Flex sm={6} xs={12}>
                    <ProductImage>
                        <Image alt={product.images[0].name} src={product.images[0].src} layout="fill" priority objectFit='cover' objectPosition="center" />
                        {product.on_sale ? <div>Sale</div> : ''}
                    </ProductImage>

                </Flex>
                <Flex sm={6} xs={12}>
                    <main>
                        <div>Shop / {product.categories.map(d => d.name).sort((a, b) => a.id - b.id).join(" / ")}</div>
                        <h2>{product.name}</h2>
                        <div dangerouslySetInnerHTML={{ __html: product.price_html }}>
                        </div>
                        <div dangerouslySetInnerHTML={{ __html: product.short_description }}></div>
                        <span>
                            <span className="material-icons" onClick={decrement}>
                                remove
                            </span>
                            <div>
                                {itemQuantity}
                            </div>

                            <span className="material-icons" onClick={increment}>
                                add
                            </span>
                        </span>
                        <Button onClick={addToCart}>ADD TO CART</Button>
                    </main>
                </Flex>
                <Flex md={12}>
                    <h3>Related Product</h3>
                </Flex>
                {
                    relatedProducts.map((product, i) => (
                        <Flex key={i} md={3} sm={6} xs={12}>
                            <Link href={`/product/${product.slug}-${product.id}`}>
                                <a>
                                    <Card product={product} priority={i < 5 ? true : false} />
                                </a>
                            </Link>
                        </Flex>
                    ))
                }
            </Flex>
        </StyledProduct>)
}