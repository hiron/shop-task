import { StyledHeader } from "./styles/Header.styled";
import Link from "next/link";
import { useRouter } from "next/router";

export const Header = ({ action, title, menus }) => {
    const router = useRouter();
    //console.log(router);
    return (
        <StyledHeader action={action}>
            <p onClick={()=> router.back()}> &lt; Back</p>
            <div>{title.substr(0,1).toUpperCase()+title.substr(1)}</div>
            <nav>{
                menus.map((menu, i) => (<Link key={i} href={`/shop/${menu.slug}`}>
                    <a>{menu.name}</a>
                </Link>))
            }
            </nav>
        </StyledHeader>
    );
}