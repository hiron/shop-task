import { Header } from "./Header"
import { Container } from "./styles/Container.styled"

export const Layout = ({ children, title, action, menus }) => {
    return (<>
        <Container>
            <Header title={title} action={action} menus={menus} />
        </Container>
        {children}
    </>)
}