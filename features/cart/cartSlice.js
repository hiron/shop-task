import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  line_items: [],
  loading: "idle",
};

export const placeOrder = createAsyncThunk("cart/placeorder", async (body) => {
  return await axios.post("/api/order", body);
});

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addItem: (state, action) => {
      let ids = state.line_items.map((d) => d.product_id);
      //   console.log(ids);

      if (ids.includes(action.payload.product_id)) {
        let ids = state.line_items.map((d) => d.product_id);
        state.line_items[ids.indexOf(action.payload.product_id)].quantity +=
          action.payload.quantity;
      } else {
        state.line_items.push(action.payload);
      }
    },
    updateQty: (state, action) => {
      if (action.payload.quantity) {
        let ids = state.line_items.map((d) => d.product_id);
        state.line_items[ids.indexOf(action.payload.product_id)].quantity =
          action.payload.quantity;
      }
    },

    deleteItem: (state, action) => {
      state.line_items = state.line_items.filter(
        (d) => d.product_id != action.payload.product_id
      );
    },
    emptyCart: (state, action) => {
      state.line_items = [];
      state.loading = "idle";
    },
  },
  extraReducers: (builder) => {
    builder.addCase(placeOrder.fulfilled, (state, action) => {
      console.log(action.payload);
      state.loading = "succeeded";
    });

    builder.addCase(placeOrder.pending, (state) => {
      state.loading = "pending";
    });

    builder.addCase(placeOrder.rejected, (state, action) => {
      console.log(action.error);
      state.loading = "failed";
    });
  },
});

export default cartSlice.reducer;

export const { addItem, deleteItem, updateQty, emptyCart } = cartSlice.actions;
