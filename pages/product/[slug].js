import axios from "axios";
import { useRouter } from "next/router";
import { WooCommerce } from "../../common/woocommerce";
import { Product } from "../../components/Product";
import { Container } from "../../components/styles/Container.styled";

export default function ProductSlug({ product, relatedProducts }) {
    //console.log(relatedProducts);
  const router = useRouter();
  //console.log(router);
  return router.isFallback ? (
    <div>Loading....</div>
  ) : (
    <Container>
        <Product product={product} relatedProducts={relatedProducts}/>
    </Container>
  );
}

export async function getStaticPaths() {
  var products = await WooCommerce.getAsync("products");
  products = JSON.parse(products.toJSON().body);

  //console.log(products);

  var paths = products.map((d) => ({
    params: { slug: d.slug+"-"+d.id },
  }));

  console.log(paths);

  return {
    paths,
    //fallback: false,
    fallback: "blocking",
  };
}

export async function getStaticProps(context) {
  console.log(context);
  const slug = context.params.slug.split("-");
  //const id = context.query.id;

  //console.log(await axios.get('/api/wc'));

  var product = await WooCommerce.getAsync(`products/${slug[slug.length - 1]}`);
  product = JSON.parse(product.toJSON().body);

  //let product = products.find((d) => d.slug == slug);

  let relatedProducts = await Promise.all(product.related_ids.map(d=> WooCommerce.getAsync("products/"+d)))
  relatedProducts = relatedProducts.map(d=> JSON.parse(d.toJSON().body));

  return {
    props: { product,  relatedProducts},
    revalidate: 60,
  };
}
