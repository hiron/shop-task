import { WooCommerce } from "../../common/woocommerce";

export default async (req, res) => {
  //console.log(req);
  const body = req.body;
  let data = await WooCommerce.postAsync("orders", body);
  //console.log(data);
  res.status(200).json({ data: JSON.parse(data.body) });
};
