import { Button } from "../components/styles/Button.styled";
import { StyledCart } from "../components/styles/Cart.styled";
import { Container } from "../components/styles/Container.styled";
import { Flex } from "../components/styles/Flex.styled";
import { useSelector, useDispatch } from "react-redux";
import { CartItem } from "../components/CartItem";
import { useRouter } from "next/router";
import { Meta } from "../components/Meta";

const Cart = () => {
  const { cart } = useSelector((state) => state);
  const router = useRouter();

  return (
    <StyledCart>
      <Meta title="Cart" description="Shop Cart"/>
      <Flex container={true}>
        <Flex>
          <h2>Cart</h2>
        </Flex>
        <Flex>
          <div>
            <table>
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {cart.line_items.map((d, i) => (
                  <CartItem key={i} product={d.product} quantity={d.quantity} />
                ))}
              </tbody>
            </table>
          </div>
        </Flex>
        <Flex>
          <span>
            <Button onClick={() => router.push("/checkout")}>
              PROCEED TO CHECKOUT
            </Button>
          </span>
        </Flex>
      </Flex>
    </StyledCart>
  );
};

export default Cart;
