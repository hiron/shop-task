import { CheckoutItem } from "../components/CheckoutItem";
import { StyledCheckout } from "../components/styles/Chekcout.styled";
import { Flex } from "../components/styles/Flex.styled";
import { useSelector, useDispatch } from "react-redux";
import { currency } from "../common/helper";
import { Button } from "../components/styles/Button.styled";
import { useEffect, useState } from "react";
import { emptyCart, placeOrder } from "../features/cart/cartSlice";
import { useRouter } from "next/router";
import { Meta } from "../components/Meta";

export default function Checkout() {
  const { cart } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const [email, setEmail] = useState("");
  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");

  useEffect(() => {
    if (cart.loading == "succeeded") {
      dispatch(emptyCart());
      router.push("/");
    }
  }, [cart.loading]);

  function handleSubmit(event) {
    event.preventDefault();
    console.log(email);
    console.log(firstname);
    console.log(lastname);
    let body = {
      line_items: cart.line_items,
      billing: {
        first_name: firstname,
        last_name: lastname,
        email,
      },
      shipping: {
        first_name: firstname,
        last_name: lastname,
      },
    };

    dispatch(placeOrder(body));
  }
  return (
    <StyledCheckout>
      <Meta title="Checkout" description="Woocommenrce Shop Checkout"/>
      <Flex Container={true}>
        <Flex>
          <h2>Checkout</h2>
        </Flex>
      </Flex>
      <Flex>
        <form onSubmit={handleSubmit}>
          <Flex container={true}>
            <Flex>
              <label htmlFor="email">Email *</label>
              <input
                type="text"
                data-id="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="email@example.com"
              ></input>
            </Flex>
          </Flex>
          <Flex container={true}>
            <Flex sm={6} xs={12}>
              <label htmlFor="firstname">First name *</label>
              <input
                type="text"
                data-id="firstname"
                value={firstname}
                onChange={(e) => setFirstName(e.target.value)}
                placeholder="John"
              ></input>
            </Flex>

            <Flex sm={6} xs={12}>
              <label htmlFor="lastname">Last name *</label>
              <input
                type="text"
                data-id="lastname"
                value={lastname}
                onChange={(e) => setLastName(e.target.value)}
                placeholder="Doe"
              ></input>
            </Flex>
          </Flex>
          <Flex>
            <table>
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {cart.line_items.map((d, i) => (
                  <CheckoutItem
                    key={i}
                    product={d.product}
                    quantity={d.quantity}
                  />
                ))}
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>
                    {currency(
                      cart.line_items
                        .map((d) => +d.quantity * +d.product.price)
                        .reduce((a, b) => a + b, 0)
                    )}
                  </td>
                </tr>
              </tfoot>
            </table>
            <div>
              <Button disabled={firstname==""|| lastname=="" || email=="" || cart.line_items.length == 0}>
                {cart.loading == "pending" ? "LOADING..." : "CONFIRM PURCHASE"}
              </Button>
            </div>
          </Flex>
        </form>
      </Flex>
    </StyledCheckout>
  );
}
