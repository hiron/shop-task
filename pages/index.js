import Link from "next/link";
import { WooCommerce } from "../common/woocommerce";
import { Card } from "../components/Card";
import { Layout } from "../components/Layout";
import { Meta } from "../components/Meta";
import { Container } from "../components/styles/Container.styled";
import { Flex } from "../components/styles/Flex.styled";

export default function Home({ menus, products }) {
  return (
    <Layout title="Shop" aciton={false} menus={menus}>
      <Meta title="Shop Home" description="Woocommenrce Shop Homepage"/>
      <Container>
        <Flex container={true}>
          {products.map((product, i) => (
            <Flex key={i} md={3} sm={6} xs={12}>
              <Link href={`/product/${product.slug}-${product.id}`}>
                <a>
                  <Card product={product} priority={i < 5 ? true : false} />
                </a>
              </Link>
            </Flex>
          ))}
        </Flex>
      </Container>
    </Layout>
  );
}

export async function getStaticProps(context) {
  var categories = await WooCommerce.getAsync("products/categories");
  var menus = JSON.parse(categories.toJSON().body).filter(
    (d) => d.parent == 0 && d.slug != "uncategorized"
  );

  var products = await WooCommerce.getAsync("products");

  products = JSON.parse(products.toJSON().body);

  return {
    props: { menus, products },
    revalidate: 60,
  };
}
