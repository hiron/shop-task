import Head from "next/head";
import { Provider } from "react-redux";
import { store } from "../app/store";
import { StyledThemeProvider } from "../styles/styled-theme-provider";

function MyApp({ Component, pageProps }) {
  return (
    <StyledThemeProvider>
      <Head>
        <link
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet"
        />
      </Head>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </StyledThemeProvider>
  );
}

export default MyApp;
