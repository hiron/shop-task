import { useRouter } from "next/router";
import { WooCommerce } from "../../common/woocommerce";
import { Card } from "../../components/Card";
import { Layout } from "../../components/Layout";
import { Container } from "../../components/styles/Container.styled";
import { Flex } from "../../components/styles/Flex.styled";
import Link from "next/link";
import { Meta } from "../../components/Meta";

export default function SlugPage({ products, menus }) {
  const route = useRouter();
  const { slug } = route.query;

  //console.log(menus);

  return route.isFallback ? (
    // !Array.isArray(slug)
    <div>Loading...</div>
  ) : (
    <Layout title={slug[slug.length - 1]} action={true} menus={menus}>
      <Meta title={"Shop|"+slug.join("|")} description={slug[slug.length - 1]+"Products Page"} />
      <Container>
        <Flex container={true}>
          {products.map((product, i) => (
            <Flex key={i} md={3} sm={6} xs={12}>
              <Link href={`/product/${product.slug}-${product.id}`}>
                <a>
                  <Card product={product} priority={i < 5 ? true : false} />
                </a>
              </Link>
            </Flex>
          ))}
        </Flex>
      </Container>
    </Layout>
  );
}

export async function getStaticPaths() {
  const categories = await WooCommerce.getAsync("products/categories");
  const parents = JSON.parse(categories.toJSON().body).filter(
    (d) => d.parent == 0
  );
  const children = JSON.parse(categories.toJSON().body).filter(
    (d) => d.parent !== 0
  );

  const paths = children.map((d) => ({
    params: { slug: [parents.find((f) => f.id == d.parent).slug, d.slug] },
  }));

  paths.push(...parents.map((d) => ({ params: { slug: [d.slug] } })));

  //setTimeout(()=>console.log(JSON.stringify(paths)), 1000);

  return {
    paths,
    fallback: "blocking",
    //fallback: false,
  };
}

export async function getStaticProps(context) {
  //console.log(context);
  const slug = context.params.slug;
  var products = await WooCommerce.getAsync("products");
  products = JSON.parse(products.toJSON().body).filter((d) => {
    let categorySlug = d.categories.map((f) => f.slug);
    return slug.every((f) => categorySlug.includes(f));
  });

  const categories = await WooCommerce.getAsync("products/categories");

  let slugId = products[0]?.categories.find(
    (d) => d.slug == slug[slug.length - 1]
  ).id;

  const menus = JSON.parse(categories.toJSON().body)
    .filter((d) => d.parent == slugId)
    .map((d) => ({ ...d, slug: slug.join("/") + "/" + d.slug }));

  console.log(products.length);
  return { props: { products, menus }, revalidate: 60 };
}
