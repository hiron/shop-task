/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  compiler:{
    styledComponents: true,
  },
  images:{
    domains: ['shop-interview.acrowd.se']
  }
}

module.exports = nextConfig
