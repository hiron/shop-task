import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "../features/cart/cartSlice";

function localStorageMiddleware({ getState }) {
  return (next) => (aciton) => {
    const result = next(aciton);
    localStorage.setItem("cart", JSON.stringify(getState()));
    return result;
  };
}

function reHydrateStore() {
  try {
    const serializedState = localStorage.getItem("cart");
    if (!serializedState) return undefined;
    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
}

export const store = configureStore({
  reducer: {
    cart: cartReducer,
  },
  devTools: true,
  preloadedState: reHydrateStore(),
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }).concat(
      localStorageMiddleware
    ),
});
